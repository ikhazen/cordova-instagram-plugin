package com.bodecontagion.cordova.plugins;

import java.io.File;
import java.io.FilenameFilter;
import java.io.FileOutputStream;
import java.io.IOException;

import android.os.Environment;
import android.net.Uri;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.util.Base64;
import android.support.v4.content.FileProvider;
import android.annotation.TargetApi;
import android.os.Build;
import android.app.Activity;
import android.util.Log;

import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.PluginResult;
import org.apache.cordova.PluginResult.Status;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

@TargetApi(Build.VERSION_CODES.FROYO)
public class BCInstagram extends CordovaPlugin {

	private static final FilenameFilter OLD_IMAGE_FILTER = new FilenameFilter() {
		@Override
		public boolean accept(File dir, String name) {
			return name.startsWith("instagram");
		}
	};

	CallbackContext cbContext;

	@Override
	public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
		this.cbContext = callbackContext;
		
		if (action.equals("share")) {
			String base64Data = args.getString(0);
			String mimetype = args.getString(1);
			String extension = args.getString(2);

			PluginResult result = new PluginResult(Status.NO_RESULT);
			result.setKeepCallback(true);

			this.share(base64Data, mimetype, extension);
			return true;
		} else if (action.equals("isInstalled")) {
			this.isInstalled();
		} else {
			callbackContext.error("Invalid Action");
		}
		return false;
	}

	private void isInstalled() {
		try {
			this.webView.getContext().getPackageManager().getApplicationInfo("com.instagram.android", 0);
			this.cbContext.success(this.webView.getContext().getPackageManager().getPackageInfo("com.instagram.android", 0).versionName);
		} catch (PackageManager.NameNotFoundException e) {
			this.cbContext.error("Application not installed");
		}
	}

	private void share(String base64Data, String type, String extension) {
		if (base64Data != null && base64Data.length() > 0) {
			byte[] baseData = Base64.decode(base64Data, 0);

			File file = null;
			FileOutputStream os = null;

			File parentDir = this.webView.getContext().getExternalFilesDir(null);
			File[] oldFiles = parentDir.listFiles(OLD_IMAGE_FILTER);
			for (File oldFile : oldFiles) {
				oldFile.delete();
			}

			try {
				file = File.createTempFile("instagram", "."+extension, parentDir);
				os = new FileOutputStream(file, true);
			} catch (Exception e) {
				e.printStackTrace();
			}

			try {
				os.write(baseData);
				os.flush();
				os.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			Intent shareIntent = new Intent(Intent.ACTION_SEND);
			shareIntent.setType(type + "/*");

			if (Build.VERSION.SDK_INT < 26) {
				// Handle the file uri with pre Oreo method
				shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.fromFile(file));
			} else {
				// Handle the file URI using Android Oreo file provider
				FileProvider FileProvider = new FileProvider();

				Uri fileUri = FileProvider.getUriForFile(this.cordova.getActivity().getApplicationContext(),
						this.cordova.getActivity().getPackageName() + ".provider", file);

				shareIntent.putExtra(Intent.EXTRA_STREAM, fileUri);
				shareIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
			}

			shareIntent.setPackage("com.instagram.android");

			this.cordova.startActivityForResult((CordovaPlugin) this, Intent.createChooser(shareIntent, "Share to"), 12345);
		} else {
			this.cbContext.error("Expected one non-empty string argument.");
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (resultCode == Activity.RESULT_OK) {
			Log.v("Instagram", "shared ok");
			if (this.cbContext != null) {
				this.cbContext.success();
			}
		} else if (resultCode == Activity.RESULT_CANCELED) {
			Log.v("Instagram", "share cancelled");
			if (this.cbContext != null) {
				this.cbContext.error("Share Cancelled");
			}
		}
	}
}
