var exec = require('cordova/exec');

var hasCheckedInstall = false;
var isAppInstalled = false;

var BCInstagram = {
	/**
	 * checks if the instagram is installed
	 * 
	 * @returns {Promise}
	 */
	isInstalled: function () {
		return new Promise(function(resolve, reject) {
			var successCb = function (version) {
				hasCheckedInstall = true
				isAppInstalled = true
				resolve(version ? version : true)
			}
			var errorCb = function (error) {
				hasCheckedInstall = false
				isAppInstalled = false
				reject({
					message: '[Error] Error on checking if Instagram is installed.',
					error: error || null
				})
			}
			exec(successCb, errorCb, 'BCInstagram', 'isInstalled', [])
		})
	},

	/**
	 * share the video and image (Android only) to Instagram and Image only on iOS
	 * 
	 * @param {string} dataUrl 
	 * @param {string} caption 
	 * @returns {Promise}
	 */
	share: function (dataUrl, caption) {
		// copy the caption.
		BCInstagram.copyToClipboard(caption)

		return new Promise(function(resolve, reject) {

			// Split the base64 string in data and contentType
			var block = dataUrl.split(";");

			if (block.length < 2) {
				reject({
					message: '[Error] DataUrl is not valid.',
					error: 'DataUrl is not valid.'
				})
			}
			// Get the content type
			var contentType = block[0].match(/image|video/) // image or video

			if (!contentType) {
				reject({
					message: '[Error] File must be a type of an Image or Video',
					error: 'file must be a type of an Image or Video'
				})
			}

			contentType = contentType[0]

			// get the file extension.
			var extension = block[0].split('/')[1]
			// get the real base64 content of the file
			var base64 = block[1].split(",")[1]

			var successCb = function () {
				resolve()
			}
			var errorCb = function (error) {
				reject({
					message: '[Error] failed sharing Video to Instagram',
					error: error
				})
			}
			exec(successCb, errorCb, 'BCInstagram', 'share', [base64, contentType, extension])
		})
	},

	/**
	 * shares the image or video to Instagram
	 * This method is for iOS only.
	 * 
	 * @param {string} url 
	 * @param {string} caption 
	 * @param {string} fileName 
	 * @param {string} folderName 
	 * @param {string} assetLocalIdentifier 
	 * @returns {Promise}
	 */
	shareAsset: function (url, caption, fileName, folderName, assetLocalIdentifier = null) {

		// copy caption
		BCInstagram.copyToClipboard(caption)

		// fileName must not have a space.
		fileName = fileName.replace(' ', '-')

		// init FileTransfer plugin
		var fileTransfer = new FileTransfer()
		var Uri = encodeURI(url)
		
		var fileDir = cordova.file.documentsDirectory
		var fullPath = fileDir + fileName

		return new Promise(function(resolve, reject) {
			// ======================== calling bridge ========================
			var successCalling = function (success) {
				resolve(success)
			}

			var failedCalling = function (error) {
				reject({
					message: '[Error] failed sharing Video to Instagram',
					error: error
				})
			}
			// ======================== end calling bridge ========================
			
			// ======================== saving ========================
			var successSaving = function (results) {
				// call bridge
				exec(successCalling, failedCalling, 'BCInstagram', 'shareAsset', [assetLocalIdentifier])
			}

			var failedSaving = function (error) {
				reject({
					message: '[Error] Failed saving Video to Library.',
					error: error
				})
			}
			// ======================== end saving ========================
			
			// ======================== downloading ========================
			var successDownload = function (result) {
				var file = result.toURL()

				// save to Library
				LibraryHelper.saveVideoToLibrary(successSaving, failedSaving, file, folderName)
			}

			var failedDownload = function (error) {
				reject({
					message: '[Error] failed downloading the Video',
					error: error
				})
			}
			// ======================== end downloading ========================

			// download
			fileTransfer.download(Uri, fullPath, successDownload, failedDownload)
			
		})
	},

	/**
	 * Copies the text caoption to clipboard
	 * 
	 * @param {String} caption 
	 * @returns {void}
	 */
	copyToClipboard: function(caption) {
		if (cordova && cordova.plugins && cordova.plugins.clipboard && caption !== '') {
			cordova.plugins.clipboard.copy(caption)
		}
	},

	/**
	 * Request Camera Roll authorization
	 * 
	 * Platform: iOS
	 * 
	 * @returns {Promise}
	 */
	requestCameraRollAuthorization: function () {
		return new Promise(function(resolve, reject) {
			var successCb = function(status) {
				resolve(status === 'authorized')
			}
			var errorCb = function(error) {
				reject(error)
			}
			exec(successCb, errorCb, 'BCInstagram', 'requestCameraRollAuthorization', []);
		})
	},

	/**
	 * Retrieves Camera Roll authorization status
	 * 
		 Platform: iOS
		 
		 @returns {Promise}
	 */
	getCameraRollAuthorizationStatus: function () {
		return new Promise(function(resolve, reject) {
			var successCb = function (status) {
				resolve(status)
			}
			var errorCb = function (error) {
				reject(error)
			}
			exec(successCb, errorCb, 'BCInstagram', 'getCameraRollAuthorizationStatus', []);
		})
	}
};

module.exports = BCInstagram;